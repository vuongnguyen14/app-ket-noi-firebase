package com.example.appfirebase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    RecyclerView rcview;
    recyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rcview=findViewById(R.id.recyclerView);
        rcview.setLayoutManager(new LinearLayoutManager(this));
        FirebaseRecyclerOptions<modelRecycler> options = new FirebaseRecyclerOptions.Builder<modelRecycler>().setQuery(FirebaseDatabase.getInstance().getReference().child("cuahang"),modelRecycler.class).build();

        adapter=new recyclerAdapter(options);
        rcview.setAdapter(adapter);
    }
    @Override
    protected void onStart(){
        super.onStart();
        adapter.startListening();

    }
    @Override
    protected void onStop(){
        super.onStop();
        adapter.stopListening();

    }
}