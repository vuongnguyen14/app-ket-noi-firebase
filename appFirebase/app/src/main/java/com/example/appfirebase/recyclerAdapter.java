package com.example.appfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class recyclerAdapter extends FirebaseRecyclerAdapter<modelRecycler,recyclerAdapter.Viewholder> {


    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public recyclerAdapter(@NonNull FirebaseRecyclerOptions<modelRecycler> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull recyclerAdapter.Viewholder holder, int position, @NonNull modelRecycler model) {

        holder.diachi.setText(model.getDiachi());
        holder.ten.setText(model.getTen());
        Glide.with(holder.anh.getContext()).load(model.getAnh()).into(holder.anh);

    }

    @NonNull
    @Override
    public recyclerAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_layout_recycler,parent,false);
        return new Viewholder(v);
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView anh;
        TextView ten,diachi;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            anh = itemView.findViewById(R.id.image);
            ten = itemView.findViewById(R.id.name);
            diachi = itemView.findViewById(R.id.address);
        }
    }
}