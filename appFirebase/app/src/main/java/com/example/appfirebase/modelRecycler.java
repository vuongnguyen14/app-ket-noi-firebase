package com.example.appfirebase;

public class modelRecycler {

    public String ten;
    public String anh;
    public String diachi;

    modelRecycler(){}

    public modelRecycler(String ten, String anh, String diachi) {
        this.ten = ten;
        this.anh = anh;
        this.diachi = diachi;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }
}
